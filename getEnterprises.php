<?php 

require_once 'conexion.php';

$sql = "SELECT idEmpresa, razonSocial 
        FROM Empresa 
        WHERE estado = 1";

$resultado = sqlsrv_query($conexion, $sql);

$empresas = array();

while( $row = sqlsrv_fetch_array( $resultado, SQLSRV_FETCH_ASSOC) ) {
    $fila = array(        
        'id'=> $row['idEmpresa'],
        'nombre'=> ucwords(utf8_encode($row['razonSocial']))
    );
    array_push($empresas, $fila);
}

echo json_encode($empresas);

?>