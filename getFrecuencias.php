<?php 

require_once 'conexion.php';

$sql = "SELECT idFrecuencia, nombreFrecuencia
        FROM UnidadMedida
        WHERE estado = 1";
$resultado = sqlsrv_query($conexion, $sql);

$frecuencias = array();

while( $row = sqlsrv_fetch_array( $resultado, SQLSRV_FETCH_ASSOC) ) {
    
    $fila = array(        
        'id'=> $row['idFrecuencia'],
        'nombre'=> utf8_encode($row['nombreFrecuencia'])
    );
    array_push($frecuencias, $fila);
}

echo json_encode($frecuencias);

?>