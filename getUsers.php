<?php 

require_once 'conexion.php';

$sql = "SELECT M.idUsuarioMovil, M.nombreUsuario, M.passwordUsuario, I.nombreInspector 
        FROM UsuarioMovil M 
        INNER JOIN Inspector I ON M.idInspector = I.idInspector 
        WHERE M.estado = 1 AND I.estado = 1";

$resultado = sqlsrv_query($conexion, $sql);

$usuarios = array();

while( $row = sqlsrv_fetch_array( $resultado, SQLSRV_FETCH_ASSOC) ) {

    $fila = array(        
        'id'=> $row['idUsuarioMovil'],
        'user'=> $row['nombreUsuario'],
        'password'=> $row['passwordUsuario'],
        'inspector'=> $row['nombreInspector']
    );
    array_push($usuarios, $fila);
}

echo json_encode($usuarios);

?>