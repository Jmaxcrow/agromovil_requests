<?php 

require_once 'conexion.php';

$sql = "SELECT idConfiguracionRecomendacion, idFundoVariedad, idCriterioRecomendacion 
        FROM ConfiguracionRecomendacion 
        WHERE estado = 1";
$resultado = sqlsrv_query($conexion, $sql);

$configuracionrecomendaciones = array();

while( $row = sqlsrv_fetch_array( $resultado, SQLSRV_FETCH_ASSOC) ) {
    
    $fila = array(        
        'id'=> $row['idConfiguracionRecomendacion'],
        'idFundoVariedad'=> $row['idFundoVariedad'],
        'idCriterioRecomendacion'=> $row['idCriterioRecomendacion']
    );
    array_push($configuracionrecomendaciones, $fila);
}

echo json_encode($configuracionrecomendaciones);

?>