<?php 

require_once 'conexion.php';

$sql = "SELECT idConfiguracionCriterio, idFundoVariedad, idCriterioInspeccion 
        FROM ConfiguracionCriterio 
        WHERE estado = 1";
$resultado = sqlsrv_query($conexion, $sql);

$configuracioncriterios = array();

while( $row = sqlsrv_fetch_array( $resultado, SQLSRV_FETCH_ASSOC) ) {
    
    $fila = array(        
        'id'=> $row['idConfiguracionCriterio'],
        'idFundoVariedad'=> $row['idFundoVariedad'],
        'idCriterioInspeccion'=> $row['idCriterioInspeccion']
    );
    array_push($configuracioncriterios, $fila);
}

echo json_encode($configuracioncriterios);

?>