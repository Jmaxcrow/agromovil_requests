<?php 

require_once 'conexion.php';

$sql = "SELECT C.idContacto, C.nombreContacto, F.idFundo
		FROM Contacto C,
			 Fundo F
		WHERE UPPER(C.tipoContacto) = UPPER(F.nombreFundo)
		AND C.estado = 1
		AND F.estado = 1";
$resultado = sqlsrv_query($conexion, $sql);

$contactos = array();

while( $row = sqlsrv_fetch_array( $resultado, SQLSRV_FETCH_ASSOC) ) {
    
    $fila = array(        
        'id'=> $row['idContacto'],
        'nombre'=> utf8_encode($row['nombreContacto']),
        'idFundo'=> $row['idFundo']
    );
    array_push($contactos, $fila);
}

echo json_encode($contactos);

?>