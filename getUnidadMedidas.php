<?php 

require_once 'conexion.php';

$sql = "SELECT idUnidadMedida, nombreUnidadMedida
        FROM UnidadMedida
        WHERE estado = 1";
$resultado = sqlsrv_query($conexion, $sql);

$unidadmedidas = array();

while( $row = sqlsrv_fetch_array( $resultado, SQLSRV_FETCH_ASSOC) ) {
    
    $fila = array(        
        'id'=> $row['idUnidadMedida'],
        'nombre'=> utf8_encode($row['nombreUnidadMedida'])
    );
    array_push($unidadmedidas, $fila);
}

echo json_encode($unidadmedidas);

?>