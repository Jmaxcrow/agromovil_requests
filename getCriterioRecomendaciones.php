<?php

require_once 'conexion.php';

$sql = "SELECT idUnidadMedida, nombreUnidadMedida
        FROM UnidadMedida
        WHERE estado = 1";
$resultado = sqlsrv_query($conexion, $sql);

$unidadmedidas = array();

while( $row = sqlsrv_fetch_array( $resultado, SQLSRV_FETCH_ASSOC) ) {
    
    array_push($unidadmedidas, utf8_encode($row['nombreUnidadMedida']));
}

$unidadmedidas = implode('-', $unidadmedidas);

$sql = "SELECT idFrecuencia, nombreFrecuencia
        FROM Frecuencia
        WHERE estado = 1";
$resultado = sqlsrv_query($conexion, $sql);

$frecuencias = array();

while( $row = sqlsrv_fetch_array( $resultado, SQLSRV_FETCH_ASSOC) ) {
    
    array_push($frecuencias, utf8_encode($row['nombreFrecuencia']));
}

$frecuencias = implode('-', $frecuencias);

$sql = "  SELECT CR.idCriterioRecomendacion, CR.nombreCriterioRecomendacion, td.nombreTipoDato, CIL.nombreLista, CR.idTipoRecomendacion
          FROM CriterioRecomendacion CR
          INNER JOIN TipoDatoCriterio TD  ON  CR.idTipoDatoCriterio = TD.idTipoDatoCriterio
          LEFT JOIN CriterioRecomendacionLista CIL ON CR.idCriterioRecomendacion = CIL.idCriterioRecomendacion
          WHERE CR.estado = 1";

$resultado = sqlsrv_query($conexion, $sql);

$criteriorecomendaciones = array();

$idCriterioRecomendacion = 0;
$lista = array();

while( $row = sqlsrv_fetch_array( $resultado, SQLSRV_FETCH_ASSOC) ) {

    if ($row['idCriterioRecomendacion'] != $idCriterioRecomendacion) {
        if ($idCriterioRecomendacion == 0) {
            $idCriterioRecomendacion = $row['idCriterioRecomendacion'];
        }
        else{
            array_push($criteriorecomendaciones, $fila);
            $idCriterioRecomendacion =  $row['idCriterioRecomendacion'];
            $lista = array();
        }
    }
    if ($row['idCriterioRecomendacion'] == $idCriterioRecomendacion) {
        array_push($lista, utf8_encode($row['nombreLista']));
        if (count($lista) > 1) {
            $fila = array(        
                    'id'=> $row['idCriterioRecomendacion'],
                    'nombre'=> utf8_encode($row['nombreCriterioRecomendacion']),
                    'tipodato'=> utf8_encode($row['nombreTipoDato']),
                    'magnitud'=> implode('-', $lista),
                    'idTipoRecomendacion'=> $row['idTipoRecomendacion'],
                    'frecuencias' => $frecuencias,
                    'unidadmedidas' => $unidadmedidas
            );
        }
        else
        {
            $fila = array(        
                    'id'=> $row['idCriterioRecomendacion'],
                    'nombre'=> utf8_encode($row['nombreCriterioRecomendacion']),
                    'tipodato'=> utf8_encode($row['nombreTipoDato']),
                    'magnitud'=> utf8_encode($row['nombreLista']),
                    'idTipoRecomendacion'=> $row['idTipoRecomendacion'],
                    'frecuencias' => $frecuencias,
                    'unidadmedidas' => $unidadmedidas
            );
        }
    }
}

echo json_encode($criteriorecomendaciones);

?>