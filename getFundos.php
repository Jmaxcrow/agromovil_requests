<?php 

require_once 'conexion.php';

$sql = "SELECT idFundo, nombreFundo, idEmpresa, areaSistemaRiego 
        FROM Fundo 
        WHERE estado = 1";
$resultado = sqlsrv_query($conexion, $sql);

$fundos = array();

while( $row = sqlsrv_fetch_array( $resultado, SQLSRV_FETCH_ASSOC) ) {
    
    $fila = array(        
        'id'=> $row['idFundo'],
        'nombre'=> utf8_encode($row['nombreFundo']),
        'areaSistemaRiego' => $row['areaSistemaRiego'],
        'idEmpresa'=> $row['idEmpresa']
    );
    array_push($fundos, $fila);
}

echo json_encode($fundos);

?>