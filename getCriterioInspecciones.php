<?php

require_once 'conexion.php';

$sql = "  SELECT CI.idCriterioInspeccion, CI.nombreCriterioInspeccion, td.nombreTipoDato, CIL.nombreLista, CI.idTipoInspeccion
          FROM CriterioInspeccion CI
          INNER JOIN TipoDatoCriterio TD  ON  CI.idTipoDatoCriterio = TD.idTipoDatoCriterio
          LEFT JOIN CriterioInspeccionLista CIL ON CI.idCriterioInspeccion = CIL.idCriterioInspeccion
          WHERE ci.estado = 1";

$resultado = sqlsrv_query($conexion, $sql);

$criterioinspecciones = array();

$idCriterioInspeccion = 0;
$lista = array();

while( $row = sqlsrv_fetch_array( $resultado, SQLSRV_FETCH_ASSOC) ) {

    if ($row['idCriterioInspeccion'] != $idCriterioInspeccion) {
        if ($idCriterioInspeccion == 0) {
            $idCriterioInspeccion = $row['idCriterioInspeccion'];
        }
        else{
            array_push($criterioinspecciones, $fila);
            $idCriterioInspeccion =  $row['idCriterioInspeccion'];
            $lista = array();
        }
    }
    if ($row['idCriterioInspeccion'] == $idCriterioInspeccion) {
        array_push($lista, utf8_encode($row['nombreLista']));
        if (count($lista) > 1) {
            $fila = array(        
                    'id'=> $row['idCriterioInspeccion'],
                    'nombre'=> utf8_encode($row['nombreCriterioInspeccion']),
                    'tipodato'=> utf8_encode($row['nombreTipoDato']),
                    'magnitud'=> implode('-', $lista),
                    'idTipoInspeccion'=> $row['idTipoInspeccion']
            );
        }
        else
        {
            $fila = array(        
                    'id'=> $row['idCriterioInspeccion'],
                    'nombre'=> utf8_encode($row['nombreCriterioInspeccion']),
                    'tipodato'=> utf8_encode($row['nombreTipoDato']),
                    'magnitud'=> utf8_encode($row['nombreLista']),
                    'idTipoInspeccion'=> $row['idTipoInspeccion']
            );
        }
    }
}

echo json_encode($criterioinspecciones);

?>