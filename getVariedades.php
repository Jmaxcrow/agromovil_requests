<?php 

require_once 'conexion.php';

$sql = "SELECT idVariedad, nombreVariedad, idCultivo 
        FROM Variedad 
        WHERE estado = 1";

$resultado = sqlsrv_query($conexion, $sql);

$variedades = array();

while( $row = sqlsrv_fetch_array( $resultado, SQLSRV_FETCH_ASSOC) ) {
    
    $fila = array(        
        'id'=> $row['idVariedad'],
        'nombre'=> utf8_encode($row['nombreVariedad']),
        'idCultivo'=> $row['idCultivo']
    );
    array_push($variedades, $fila);
}

echo json_encode($variedades);

?>