<?php 

require_once 'conexion.php';


$sql = "SELECT idCultivo, nombreCultivo 
        FROM Cultivo 
        WHERE estado = 1";
$resultado = sqlsrv_query($conexion, $sql);

$cultivos = array();

while( $row = sqlsrv_fetch_array( $resultado, SQLSRV_FETCH_ASSOC) ) {

    $fila = array(        
        'id'=> $row['idCultivo'],
        'nombre'=> utf8_encode($row['nombreCultivo'])
    );
    array_push($cultivos, $fila);
}

echo json_encode($cultivos)

?>