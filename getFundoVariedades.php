<?php 

require_once 'conexion.php';

$sql = "SELECT idFundoVariedad, idFundo, idVariedad, areaPlantada, areaProduccion, plantasPorHectarea 
        FROM FundoVariedad 
        WHERE estado = 1";
$resultado = sqlsrv_query($conexion, $sql);

$fundovariedades = array();

while( $row = sqlsrv_fetch_array( $resultado, SQLSRV_FETCH_ASSOC) ) {

    $fila = array(        
        'id'=> $row['idFundoVariedad'],
        'areaPlantada' => $row['areaPlantada'],
        'areaProduccion' => $row['areaProduccion'],
        'plantasPorHectarea' => $row['plantasPorHectarea'],
        'idFundo'=> $row['idFundo'],
        'idVariedad'=> $row['idVariedad'],
    );
    array_push($fundovariedades, $fila);
}

echo json_encode($fundovariedades)

?>