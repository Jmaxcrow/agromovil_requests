<?php 
	
	require 'conexion.php';

	$return_inspeccionEvidencias = array();

	$usuario = $_POST['usuario'];
	$visitas = $_POST['visitas'];
	$evaluaciones = $_POST['evaluaciones'];
	$muestras = $_POST['muestras'];
	$fotos = $_POST['fotos'];
	$recomendaciones = $_POST['recomendaciones'];

	$usuario = json_decode($usuario, true);
	$visitas = json_decode($visitas, true);
	$evaluaciones = json_decode($evaluaciones, true);
	$muestras = json_decode($muestras, true);
	$fotos = json_decode($fotos, true);
	$recomendaciones = json_decode($recomendaciones, true);

	//var_dump($usuario);
	//var_dump($visitas);
	//var_dump($evaluaciones);
	//var_dump($muestras);
	//var_dump($fotos);

	$user = $usuario['user'];
	$password = $usuario['password'];

	//echo "$user $password";

	$sql = "SELECT idUsuarioMovil, idInspector 
			FROM UsuarioMovil 
			WHERE nombreUsuario = '$user' AND passwordUsuario = '$password'";

	$resultado = sqlsrv_query($conexion, $sql);

	$row = sqlsrv_fetch_array( $resultado, SQLSRV_FETCH_ASSOC);

	$idUser = $row['idUsuarioMovil'];
	$idInspector = $row['idInspector'];

	for ($i=0; $i < count($visitas); $i++) {

		$fila = $visitas[$i];
		$idVisita = $fila['id'];
		$idEmpresa = $fila['idEmpresa'];
		$idFundo = $fila['idFundo'];
		$idCultivo = $fila['idCultivo'];
		$idVariedad = $fila['idVariedad'];
		$fechaHora = $fila['fechaHora'];
		$fechaHora = $fila['fechaHora'];
		$lat = $fila['lat'];
		$lon = $fila['lon'];
		$contactoPersonalizado = utf8_encode($fila['contactoPersonalizado']);
		//echo $idVisita;
		$sql = "INSERT INTO Inspeccion(idEmpresa, idFundo, idCultivo, idVariedad, idInspector, idUsuarioMovil, fechaInspeccion, horaInicioInspeccion, latitudInicioInspeccion, 											longitudInicioInspeccion, contactoInspeccion, horaSincronizacionMovil) 
				VALUES ($idEmpresa, $idFundo, $idCultivo, $idVariedad, $idInspector, $idUser, '$fechaHora', '$fechaHora', $lat, $lon, '$contactoPersonalizado', CURRENT_TIMESTAMP)";
		$resultado = sqlsrv_query($conexion, $sql);

		$sql = "SELECT idInspeccion FROM Inspeccion WHERE fechaInspeccion = '$fechaHora'";
		$resultado = sqlsrv_query($conexion, $sql);

		$row = sqlsrv_fetch_array( $resultado, SQLSRV_FETCH_ASSOC);

		$idInspeccion = $row['idInspeccion'];
		//echo "idInspeccion: $idInspeccion";
		//{"id":1,"idTipoInspeccion":1,"idVisita":1,"lat":-8045.56,"lon":262.9,"porcentaje":0,"timeIni":"2018-12-06 15:26:15"},
		for ($j=0; $j < count($evaluaciones); $j++) { 

			$fila2 = $evaluaciones[$j];
			$idEvaluacion = $fila2['id'];
			$idVisita2 = $fila2['idVisita'];
			if ($idVisita != $idVisita2) {
				continue;
			}
			$idTipoInspeccion = $fila2['idTipoInspeccion'];
			$lat = $fila2['lat'];
			$lon = $fila2['lon'];
			$porcentaje = $fila2['porcentaje'];
			$timeIni = $fila2['timeIni'];
			//{"coment":"","idCriterio":2,"idEvaluacion":1,"idTipoInspeccion":1,"ids":1,"statusComent":false,"time":"2018-12-06 15:26:20","value":"56","id":0,"idTipoInspseccion":1,"magnitud":"","name":"Temperatura máxima","type":"float"},
			for ($k=0; $k < count($muestras); $k++) { 

				$fila3 = $muestras[$k];
				$idMuestra = $fila3['ids'];
				$idEvaluacion2 = $fila3['idEvaluacion'];
				if ($idEvaluacion != $idEvaluacion2) {
					continue;
				}
				$idCriterio = $fila3['idCriterio'];
				$value = $fila3['value'];
				$coment = $fila3['coment'];

				$sql = "INSERT INTO InspeccionDetalle(idInspeccion, idTipoInspeccion, idCriterioInspeccion, valorCriterio, comentariosCriterio) 
						VALUES ($idInspeccion, $idTipoInspeccion, $idCriterio, $value, '$coment')";

				$resultado = sqlsrv_query($conexion, $sql);

				$sql = "SELECT idInspeccionDetalle FROM InspeccionDetalle WHERE idInspeccion = $idInspeccion AND idTipoInspeccion = $idTipoInspeccion AND idCriterioInspeccion = $idCriterio AND valorCriterio = $value AND comentariosCriterio = '$coment'";
				$resultado = sqlsrv_query($conexion, $sql);

				$row = sqlsrv_fetch_array( $resultado, SQLSRV_FETCH_ASSOC);				

				$idInspeccionDetalle = $row['idInspeccionDetalle'];
				/*{"IdMuestra":3,"fechaHora":"2018-12-06 15:49:58","id":1,"path":"/storage/emulated/0/misImagenes/misFotos/15441293910.jpg"}*/
				for ($l=0; $l < count($fotos); $l++) { 
					$date = new DateTime();
					$date = $date->format('Y-m-d H:i:s');
					$fila4 = $fotos[$l];
					$idMuestra2 = $fila4['IdMuestra'];
					$idFoto = $fila4['id'];
					if ($idMuestra2 != $idMuestra) {
						continue;
					}
					$path = $fila4['path'];

					$sql = "INSERT INTO InspeccionEvidencia(idInspeccionDetalle, fechaHora) 
						VALUES ($idInspeccionDetalle, '$date')";
					$resultado = sqlsrv_query($conexion, $sql);

					$sql = "SELECT idInspeccionEvidencia FROM InspeccionEvidencia WHERE idInspeccionDetalle = $idInspeccionDetalle AND fechaHora = '$date'";
					$resultado = sqlsrv_query($conexion, $sql);

					$row = sqlsrv_fetch_array( $resultado, SQLSRV_FETCH_ASSOC);

					$idInspeccionEvidencia = $row['idInspeccionEvidencia'];

					array_push($return_inspeccionEvidencias, array("idInspeccionEvidencia" =>$idInspeccionEvidencia, "idFoto" => $idFoto));

				}
			}
		}

		//[{"cantidad":"","comentario":"","frecuencia":0,"idCriterioRecomendacion":1,"idVisita":1,"ids":1,"unidad":0,"id":0,"idTipoRecomendacion":1,"listFrecuancias":"UNICA VEZ-DIARIO-SEMANAL-MENSUAL","listUnidades":"KG/HA-GR/ARBOL-SACOS/HA-ML/200LT-ML/MOCHILA-%-GR/200LT-CILINDRO/HA-MOCHILA/HA-LT/HA","name":"Ácido Bórico"}

		//echo count($recomendaciones);

		for ($n=0; $n < count($recomendaciones); $n++) { 
			$fila5 = $recomendaciones[$i];
			$idVisita3 = $fila5['idVisita'];
			if ($idVisita != $idVisita3) {
				continue;
			}

			$listUnidades = explode("-", $fila5['listUnidades']);
			$index_unidades = $fila5['unidad'];
			$unidad = $listUnidades[$index_unidades];

			//echo $unidad;

			$listFrecuancias = explode("-", $fila5['listFrecuancias']);
			$index_frecuencia = $fila5['frecuencia'];
			$frecuencia = $listUnidades[$index_frecuencia];

			//echo $frecuencia;

			$idTipoRecomendacion = $fila5['idTipoRecomendacion'];
			$idCriterioRecomendacion = $fila5['idCriterioRecomendacion'];
			$cantidad = $fila5['cantidad'];

			$sql = "INSERT INTO InspeccionRecomendacion(idInspeccion, idTipoRecomendacion, idCriterioRecomendacion, valorCriterio, unidadMedida, frecuencia, comentariosCriterio)
					VALUES ($idInspeccion, $idTipoRecomendacion, $idCriterioRecomendacion, $cantidad, '$unidad', '$frecuencia', '$comentario')";
			$resultado = sqlsrv_query($conexion, $sql);
		}

	}

	if (count($return_inspeccionEvidencias) == 0) {
		echo json_encode(array("success" => 0));		
	}
	else{
		echo json_encode(array("success" => 1 , "data" => $return_inspeccionEvidencias));
	}
?>