<?php 

require_once 'conexion.php';

$sql = "SELECT idTipoRecomendacion, nombreTipoRecomendacion
        FROM TipoRecomendacion 
        WHERE estado = 1";
$resultado = sqlsrv_query($conexion, $sql);

$tipoinspecciones = array();

while( $row = sqlsrv_fetch_array( $resultado, SQLSRV_FETCH_ASSOC) ) {
    
    $fila = array(        
        'id'=> $row['idTipoRecomendacion'],
        'nombre'=> utf8_encode($row['nombreTipoRecomendacion'])
    );
    array_push($tipoinspecciones, $fila);
}

echo json_encode($tipoinspecciones);

?>